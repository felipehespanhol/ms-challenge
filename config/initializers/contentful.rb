ContentfulModel.configure do |config|
  config.access_token = ENV['CONTENTFUL_ACCESS_TOKEN'] # Required
  config.space = ENV['CONTENTFUL_SPACE_ID'] # Required
  #config.preview_access_token = "your preview token in here" # Optional - required if you want to use the preview API
  #config.management_token = "your management token in here" # Optional - required if you want to update or create content
  #config.environment = "master" # Optional - defaults to 'master'
  #config.default_locale = "en-US" # Optional - defaults to 'en-US'
  #config.options = { # Optional
  #  #extra options to send to the Contentful::Client
  #}
end
