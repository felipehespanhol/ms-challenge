# Marley Spoon Challenge

Deployed application can be visited at https://ms-challenge.herokuapp.com

## Dependencies:

* PostgreSQL
* Ruby 2.6.4

## First run

`bundle install`
`bundle exec rake db:create db:migrate`

## Starting the server

`rails s` to start at localhost:3000.

OR

To run the server with webpacker: `foreman -f Procfile.dev`. This will start rails server on port 5000.

## Running the tests:

`bundle exec rspec spec` or simply `rake`
