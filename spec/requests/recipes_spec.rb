require 'rails_helper'

RSpec.describe 'recipes', type: :request do
  describe '#index' do
    let(:recipes) { Recipe.all.load }

    it 'lists recipes' do
      get '/'
      recipes.each do |recipe|
        expect(response.body).to include(recipe.photo.url)
        expect(response.body).to include(CGI::escapeHTML(recipe.title))
      end
    end
  end

  describe '#show' do
    let(:recipe) { Recipe.first }

    it 'details a recipe' do
      get "/recipes/#{recipe.id}"
      expect(response.body).to include(recipe.photo.url)
      expect(response.body).to include(CGI::escapeHTML(recipe.title))
      expect(response.body).to include(recipe.description)
      expect(response.body).to include(recipe.chef.name)
      recipe.tags.each do |tag|
        expect(response.body).to include(tag.name)
      end
    end
  end
end
